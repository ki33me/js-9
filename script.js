/*
Опишіть, як можна створити новий HTML тег на сторінці.
метод document.createElement("")
Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

'beforebegin': до самого element (до открывающего тега).
'afterbegin': сразу после открывающего тега element (перед первым потомком).
'beforeend': сразу перед закрывающим тегом element (после последнего потомка).
'afterend': после element (после закрывающего тега).


  -beforebegin
 <element> 
    -afterbegin


    -beforeend
 </element>
-afterend


Як можна видалити елемент зі сторінки?
elem.remove()  метод remove 

Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;


Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];



["1", "2", "3", "sea", "user", 23];



Можна взяти будь-який інший масив.
*/

let arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];

let testDiv = document.querySelector(".test");

function ArraysToList(arr, parent = document.body) {
    // debugger
    function array(arr) {
        return arr.map(elem => {
            return ( !Array.isArray(elem)) ? `<li>${elem}</li>` : `<ul>${array(elem)}</ul>`;
        }).join(" ");
        //join объединяет все элементы массива (или массивоподобного объекта) в строку.
    }
    return parent.innerHTML = `<ul>${array(arr)}</ul>`;
}

ArraysToList(arr1, testDiv);


